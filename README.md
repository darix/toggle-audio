# toggle-audio

Just a small python script to toggle between 2 audio devices

## Needs

- [notify2](https://pypi.org/project/notify2/)
- [pulsectl](https://pypi.org/project/pulsectl/)

## TODO

- config file support
- cmdline arguments
- maybe wrap the whole thing in a class
