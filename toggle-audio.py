#!/usr/bin/python3
#
# GPL-3.0 (see LICENSE file)
#

import sys
import pulsectl
import notify2
# For debugging:
# import IPython
# Then use IPython.embed() when you want a shell in your program at that position.

def show_notification(title, message, icon='audio-card-symbolic'):
    n = notify2.Notification(title, message, icon)
    n.show()

def get_sink_by_index(index):
    for sink in pulse.sink_list():
        if index == sink.index:
            return sink

def get_next_sink(current):
    size = len(pulse.sink_list())
    if size < 2:
        return current
    else:
        # indexes start at 1...
        index = (current.index+1) % size
        return get_sink_by_index(index)

# Config
short_device_name = False;

# Audio sinks
HEADSET  = None
SPEAKERS = None

notify2.init('Pulseaudio Toggle Device')
notification_title = "Toggle Audio Device"

pulse = pulsectl.Pulse()

# Get current audio sink name
current_sink_name = pulse.server_info().default_sink_name

# Determine our next audio sink
# TODO add config for a proper default
if current_sink_name == HEADSET:
    new_sink_name = SPEAKERS
else:
    new_sink_name = HEADSET

# Find sink from sink list to get full object for default_set
current_sink = pulse.get_sink_by_name(current_sink_name)
new_sink     = None

try:
    if new_sink_name == None:
        new_sink = get_next_sink(current_sink)
    else:
        new_sink = pulse.get_sink_by_name(new_sink_name)
except pulsectl.PulseIndexError:
    notification_message = "Can not find the new sink '%s'. " % new_sink_name
    show_notification(notification_title, notification_message + "Check console message for more details")

    notification_message += "\nAvailable sinks are:\n\n"
    for sink in pulse.sink_list():
        notification_message += " - %s\n" % sink.name
    notification_message += "\nPlease update your device configuration with the correct devices from the list above. Exiting...\n"
    sys.stderr.write(notification_message)
    sys.exit(1)

if new_sink:

    # Set our new default device
    pulse.default_set(new_sink)

    # Move currently running streams
    for input_stream in pulse.sink_input_list():
        # TODO: only move stream that were on the current_sink.
        #       with my steelseries it will also move the chat
        #       device which is not what I want
        pulse.sink_input_move(input_stream.index, new_sink.index)

    if short_device_name and HEADSET != None and SPEAKERS != None:
        show_notification(notification_title, "Switched to %s" % ("Headset" if new_sink.name == HEADSET else "Speakers"))
    else:
        show_notification(notification_title, "Switched to device %s" % new_sink.description)
